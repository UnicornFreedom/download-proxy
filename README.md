## download-relay
Little proxy server, which will transfer to you the binary content of a
GET response from any URL you provide.

Created by me to be able to download source code files from GitLab with
AJAX requests.

Which I'm not able to do in a human way (like in GitHub), because of one
little HTTP header missing.

### Usage
Just compile it and run.

You will get a working instance on `127.0.0.1:9080`.

Use it like this:

```
127.0.0.1:9080/?url=<your URL>
```
