extern crate actix_web;
extern crate futures;
#[macro_use]
extern crate serde_derive;

use actix_web::{
    client, http, middleware, server,
    App, AsyncResponder, Body, FutureResponse, HttpMessage, HttpResponse, Query
};
use actix_web::error::ErrorNotFound;
use futures::Future;
use futures::Stream;


#[derive(Deserialize)]
struct Params {
    url: String
}

fn download(params: Query<Params>) -> FutureResponse<HttpResponse> {
    println!("> {}", params.url);
    client::get(&params.url)
        .finish().unwrap()
        .send()
        .map_err(|err| {
            println!("  ^ {}", err);
            ErrorNotFound(err)
        })
        .and_then(|response| {
            let mut result = HttpResponse::build(response.status());
            for (key, value) in response.headers() {
                result.header(key.clone(), value.clone());
            }
            result.header("Access-Control-Allow-Origin", "*");
            Ok(result.body(Body::Streaming(Box::new(response.payload().from_err()))))
        })
        .responder()
}

fn main() {
    println!("Starting the relay on 127.0.0.1:9080 ...");
    server::new(|| {
        App::new()
            .middleware(middleware::Logger::default())
            .resource("/", |r| r.method(http::Method::GET).with(download))
    })
        .bind("127.0.0.1:9080")
        .expect("Can not bind to port 9080")
        .run();
}
